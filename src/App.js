// import React from 'react';
// import Demo from './components/demo';
// import Button from './components/Button';
//
// class App extends React.Component{
//   state= {
//     count: 0,
//     name:"React"
//   }
//
//
//  incrementCount(){
//    let {count} = this.state;
//    this.setState({
//     count:count+1
//        })
//  }
//
//   render(){
//       return(
//           <div>
//               <Demo name={this.state.name}/>
//             </div>
//         )
//   }
// }
//
// export default Demo;
import React, { Component } from 'react';
import Button from './components/Button';
export default class App extends Component {

  constructor(){
    super();
    this.state= {
      count:0
    }
  }

  incrementCount = () => {
    this.setState({
      count:this.state.count+1
    })
  }

  decrementCount= () => {
    this.setState({
      count:this.state.count-1
    })
  }

  render() {
    let { count } =this.state
    return (
        <div>
          <h2>Count: { count } </h2>
          <Button
              title = { "+" }
              task = { () => this.incrementCount() }
          />
          <Button
              title = { "-" }
              task = { () => this.decrementCount() }
          />
        </div>
    );
  }

}